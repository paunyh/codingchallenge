package main

import (
	"fmt"
	"os"
	"strconv"
)

var euroCoins = []int{200, 100, 50, 20, 10, 5, 2, 1}
var ukCoins = []int{30, 24, 12, 6, 3, 1}

func main() {
	input, _ := strconv.Atoi(os.Args[2])

	if os.Args[1] == "euro" {
		solveProblem(input, euroCoins)
	} else {
		solveProblem(input, ukCoins)
	}
}

func solveProblem(input int, coins []int) []int {
	endresult := []int{}

	for i := 0; i < len(coins); i++ {
		//rotate the coins array
		coins = append(coins[1:], coins[0])
		result := giveChange(coins, input)
		if len(endresult) == 0 || numberOfCoins(result) < numberOfCoins(endresult) {
			// need to rotate result back
			for j := 0; j < len(coins)-(i+1); j++ {
				result = append(result[1:], result[0])
			}
			endresult = result
		}
	}
	fmt.Printf("Coin array:  %v\n", coins)
	fmt.Printf("Result:      %v\n", endresult)
	return endresult
}

func giveChange(coins []int, input int) []int {
	result := []int{}
	for _, x := range coins {
		if input/x > 0 {
			result = append(result, input/x)
			input = input - ((input / x) * x)
		} else {
			result = append(result, 0)
		}
	}
	fmt.Printf("Number of coins = %v result = %v\n", numberOfCoins(result), result)
	return result
}

func numberOfCoins(result []int) int {
	sum := 0
	for _, a := range result {
		sum = sum + a
	}
	return sum
}
